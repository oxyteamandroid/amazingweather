/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.weather.db.WeatherDbHelper;

/**
 * 天气应用
 * 
 * @author tZhang
 * 
 */
public class RefreshUtils {

    private static final String TAG = RefreshUtils.class.getSimpleName();

    private static WeatherModel mWeatherModel;
    private static WeatherDbHelper mDbHelper;
    public static boolean sChannelAvailable;

    private static Listener mListener;

    public static void refresh(Context context, Listener listener) {

        mListener = listener;
        initHandler(context);
        if (null == mWeatherModel) {
            mWeatherModel = WeatherModel.getInstance(context);
            mWeatherModel.startTransaction(mHandler);

            // 实例化数据库助手
            mDbHelper = new WeatherDbHelper(context.getApplicationContext(), "WATCH_WEATHER_DB", null, 1);

            // 停止通讯服务
            // if (mWeatherModel != null) {
            // mWeatherModel.stopTransaction();

        } else {
            mWeatherModel.requestRefresh();
        }
    }

    private static void initHandler(final Context context) {
        mHandler = new Handler() {
            public void handleMessage(Message msg) {

                int what = msg.what;
                sChannelAvailable = true;

                switch (what) {
                case WeatherModel.WEATHER_DISCONNECTED_STATE:
                    // 未连接/断开连接
                    sChannelAvailable = false;
                    break;

                case WeatherModel.WEATHER_REFRESH_FAILED_STATE:
                    break;

                case WeatherModel.WEATHER_REFRESH_OK_STATE:
                    // 刷新成功 接收到数据，保存到数据库
                    int intsertRow = mDbHelper.saveWeathersToDB(context.getApplicationContext().getResources(), ((WeatherInfoArray) (msg.obj)).data);
                    // 保存失败
                    if (intsertRow != 5) {
                        IwdsLog.d(TAG, "WEATHERINFO INSERT DATABASE FAILED!");
                    } else {
                        if (null != mListener)
                            mListener.onUpdate();
                    }
                    break;

                case WeatherModel.WEATHER_REQUEST_STATE:
                    // 请求天气数据
                    mWeatherModel.requestRefresh();
                    break;

                case WeatherModel.WEATHER_RESULT_TIMEOUT_STATE:
                case WeatherModel.WEATHER_SEND_RESULT_FAILED_STATE:
                case WeatherModel.WEATHER_SEND_RESULT_OK_STATE:
                default:
                    break;
                }

            };
        };
    }

    private static Handler mHandler;

    public static interface Listener {
        public void onUpdate();
    }

}
