/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray.WeatherInfo;
import com.ingenic.weather.utils.WeatherUtils;

/**
 * 可以通过SQLiteOpenHelper的以下两个方法来或得SQLiteDatabase的对象： getReadableDatabase()
 * 创建或者打开一个查询数据库 getWritableDatabase() 创建或者打开一个可写数据库
 * @author tZhang
 */
public class WeatherDbHelper extends SQLiteOpenHelper {

    private SQLiteDatabase mDataBase = null;
    private Cursor mCursor = null;
    public static final String WEATHER_TABLE_NAME = "ingneic_watch_weather";

    /**
     * 构造函数，必须实现
     * @param context
     *            上下文对象
     * @param name
     *            数据库名称
     * @param factory
     *            可选游标工厂，通常为NULL
     * @param version
     *            当前数据库版本号
     */
    public WeatherDbHelper(Context context, String name, CursorFactory factory,
            int version) {
        super(context, name, factory, version);
        mDataBase = this.getWritableDatabase();
    }

    /**
     * 数据库第一次创建时会调用，一般在其中创建数据库表
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // 使用execSQL()方法执行SQL语句，如果没有异常，这个方法没有返回值
        db.execSQL("CREATE TABLE "
                + WEATHER_TABLE_NAME
                + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "city TEXT, date TEXT, dayOfWeek TEXT, updateTime TEXT, weatherCode TEXT, "
                + "weather TEXT, tempUnit TEXT, currentTemp INTEGER, minimumTemp INTEGER, maximumTemp INTEGER, dayIndex INTEGER)");
    }

    /**
     * 当数据库需要修改的时候，Android系统会主动的调用这个方法。
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /**
     * 从数据库获取天气信息
     * @return Cursor
     */
    public Cursor getWeathersCursorFromDB() {

        if (mDataBase != null) {
            mCursor = mDataBase.query(WEATHER_TABLE_NAME, null, null, null,
                    null, null, null);
        }

        return mCursor;
    }

    /**
     * 从数据库获取天气信息
     * @return ArrayList<WeatherInfo>
     */
    public WeatherInfo[] getWeathersArrayFromDB() {

        mCursor = getWeathersCursorFromDB();

        int count = mCursor.getCount();
        if (count == 0) {
            return null;
        }

        WeatherInfo[] infos = new WeatherInfo[count];

        int i = 0;
        if (mCursor != null) {
            while (mCursor.moveToNext()) {
                infos[i] = new WeatherInfo();
                infos[i].city = mCursor.getString(mCursor
                        .getColumnIndex("city"));
                infos[i].date = mCursor.getString(mCursor
                        .getColumnIndex("date"));
                infos[i].dayOfWeek = mCursor.getString(mCursor
                        .getColumnIndex("dayOfWeek"));
                infos[i].updateTime = mCursor.getString(mCursor
                        .getColumnIndex("updateTime"));
                infos[i].weatherCode = mCursor.getString(mCursor
                        .getColumnIndex("weatherCode"));
                infos[i].weather = mCursor.getString(mCursor
                        .getColumnIndex("weather"));
                infos[i].tempUnit = mCursor.getString(mCursor
                        .getColumnIndex("tempUnit"));
                infos[i].currentTemp = mCursor.getInt(mCursor
                        .getColumnIndex("currentTemp"));
                infos[i].minimumTemp = mCursor.getInt(mCursor
                        .getColumnIndex("minimumTemp"));
                infos[i].maximumTemp = mCursor.getInt(mCursor
                        .getColumnIndex("maximumTemp"));
                infos[i].dayIndex = mCursor.getInt(mCursor
                        .getColumnIndex("dayIndex"));
                if (i < count) {
                    i++;
                }

            }
        }

        return infos;
    }

    /**
     * 保存天气信息到数据库
     */
    public int saveWeathersToDB(Resources resources, WeatherInfo[] infos) {

        int insertCount = 0;

        ContentValues values = new ContentValues();

        if (mDataBase != null && infos != null && infos.length > 0) {
            int count = infos.length;

            removeWeathersFromDB();

            long result = -1;
            for (int i = 0; i < count; i++) {

                values.clear();

                values.put("city", infos[i].city);
                values.put("date", infos[i].date);
                values.put("dayOfWeek", infos[i].dayOfWeek);
                values.put("updateTime", infos[i].updateTime);
                values.put("weatherCode", infos[i].weatherCode);
                values.put("weather", WeatherUtils.findWeather(resources, infos[i].weatherCode));
                values.put("tempUnit", infos[i].tempUnit);
                values.put("currentTemp", infos[i].currentTemp);
                values.put("minimumTemp", infos[i].minimumTemp);
                values.put("maximumTemp", infos[i].maximumTemp);
                values.put("dayIndex", infos[i].dayIndex);

                result = mDataBase.insert(WEATHER_TABLE_NAME, null, values);

                if (result == -1) {
                    break;
                }

                insertCount++;
            }
        }

        return insertCount;

    }

    /**
     * 清除数据库中的所有天气信息
     */
    void removeWeathersFromDB() {
        if (mDataBase != null) {
            mDataBase.delete(WEATHER_TABLE_NAME, null, null);
        }
    }

    /**
     * 释放数据库资源
     */
    public void closeDateBase() {
        if (mCursor != null) {
            mCursor.close();
            mCursor = null;
        }

        if (mDataBase != null) {
            mDataBase.close();
            mDataBase = null;
        }
    }

}
