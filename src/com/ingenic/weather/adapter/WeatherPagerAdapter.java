/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray;
import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray.WeatherInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.weather.R;
import com.ingenic.weather.WeatherActivity;
import com.ingenic.weather.WeatherLineChartActivity;
import com.ingenic.weather.utils.WeatherUtils;

/**
 * 天气PagerAdapter
 * 
 * @author tZhang
 * 
 */
public class WeatherPagerAdapter extends BaseAdapter {

    public static final String TAG = "WeatherPagerAdapter";

    private Context mContext;

    private WeatherInfo[] mForecasts;

    private String mDegress;

    private RightScrollView mView;

    private final int TODAY_TYPE = 0;
    private final int OTHER_TYPE = 1;
    private final int VIEW_TYPE_COUNT = 2;

    public WeatherPagerAdapter(Context context, RightScrollView view) {
        this.mContext = context;

        this.mView = view;
    }

    public void setList(WeatherInfo[] forecasts) {
        if (forecasts != null && forecasts.length > 0) {

            mForecasts = forecasts;

            // 气温单位
            if ("c".equals(mForecasts[0].tempUnit)) {
                mDegress = mContext.getString(R.string.sheshidu);
            } else {
                mDegress = mContext.getString(R.string.huashidu);
            }

            IwdsLog.d(this, "degress = " + mDegress);
        } else {
            IwdsLog.e(TAG, "No Weather Datas");
        }
    }

    @Override
    public int getItemViewType(int position) {
        int t = 0;

        if (mForecasts != null && mForecasts[0].dayIndex != 0) {
            t = 1;
        }

        return position == t ? TODAY_TYPE : OTHER_TYPE;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getCount() {
        if (mForecasts == null) {
            return 0;
        }
        return mForecasts.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        final ViewHolder holder;

        // if (convertView == null) {
        holder = new ViewHolder();

        IwdsLog.d("type", "type = " + type);

        switch (type) {
        case TODAY_TYPE:
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.weather_today_preview_pager, parent, false);
            holder.cur_temp = (TextView) convertView
                    .findViewById(R.id.cur_temp);
            break;

        case OTHER_TYPE:
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.weather_preview_pager, parent, false);
            break;
        default:
            break;
        }

        holder.week = (TextView) convertView.findViewById(R.id.weather_week);

        holder.weather_info = (TextView) convertView
                .findViewById(R.id.weather_info);

        holder.weather_icon = (ImageView) convertView
                .findViewById(R.id.weather_icon);

        holder.temp_range = (TextView) convertView
                .findViewById(R.id.temp_range);

        holder.date = (TextView) convertView.findViewById(R.id.weather_date);

        convertView.setTag(holder);

        // } else {
        //
        // holder = (ViewHolder) convertView.getTag();
        // }

        final WeatherInfo info = mForecasts[position];

        IwdsLog.d(TAG, "holder.cur_temp = " + holder.cur_temp
                + " info.currentTemp = " + info.currentTemp);

        // 设置当前温度
        if (info.dayIndex == 0) {
            holder.cur_temp.setText(info.currentTemp + mDegress);
        }

        // 设置星期
        holder.week.setText(info.dayOfWeek);

        // 设置天气状况
        holder.weather_info.setText(info.weather);

        // 设置天气图标
        holder.weather_icon.setImageResource(WeatherUtils
                .findImage(info.weatherCode));

        // 设置温度范围
        holder.temp_range.setText(info.minimumTemp + "~" + info.maximumTemp
                + mDegress);

        // 设置日期
        holder.date.setText(info.date);

        if (WeatherActivity.sChannelAvailable || getCount() > 0) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!WeatherUtils.isFastDoubleClick()) {
                        // 设置右滑不能退出首届面
                        if (mView != null) {
                            mView.disableRightScroll();
                        }

                        // 跳转到气温折线图界面
                        Intent intent = new Intent(mContext,
                                WeatherLineChartActivity.class);
                        intent.setAction(WeatherLineChartActivity.LINE_CHART_ACTION);
                        intent.putExtra(
                                WeatherLineChartActivity.LINE_CHART_INFO,
                                new WeatherInfoArray(mForecasts));
                        mContext.startActivity(intent);
                    }
                }
            });
        }
        return convertView;
    }

    class ViewHolder {

        /**
         * 当前温度
         */
        TextView cur_temp;

        /**
         * 星期
         */
        TextView week;

        /**
         * 天气状况
         */
        TextView weather_info;

        /**
         * 天气图标
         */
        ImageView weather_icon;

        /**
         * 温度范围
         */
        TextView temp_range;

        /**
         * 日期
         */
        TextView date;

    }

    public void notifyDataChanged(WeatherInfo[] forecasts) {
        mForecasts = forecasts;
        notifyDataSetChanged();
    }

}
