package com.ingenic.weather.widget;

import java.util.HashSet;
import java.util.Set;

import android.view.View;

import com.ingenic.iwds.appwidget.WidgetProvider;
import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray.WeatherInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.LocalRemoteViews;
import com.ingenic.weather.R;
import com.ingenic.weather.RefreshUtils;
import com.ingenic.weather.RefreshUtils.Listener;
import com.ingenic.weather.db.WeatherDbHelper;
import com.ingenic.weather.utils.WeatherUtils;

public class WeatherWidgetProvider extends WidgetProvider {

    private static final String TAG = WeatherWidgetProvider.class.getSimpleName();

    private LocalRemoteViews mRemoteViews;

    private WeatherDbHelper mDbHelper;

    private WeatherInfo[] mForecasts;

    private final int TODAY_TYPE = 0;
    private final int OTHER_TYPE = 1;

    @Override
    protected void onEnabled() {
        super.onEnabled();
        mDbHelper = new WeatherDbHelper(getApplicationContext(), "WATCH_WEATHER_DB", null, 1);
        IwdsLog.i(this, "Provider is enabled");
    }

    @Override
    protected LocalRemoteViews onCreateRemoteViews() {
        IwdsLog.i(this, "onCreateRemoteViews()");
        if (mRemoteViews == null) {
            IwdsLog.i(this, "create");
            mRemoteViews = new LocalRemoteViews(getPackageName(), R.layout.widget_weather);
        }

        RefreshUtils.refresh(getApplicationContext(), new Listener() {

            @Override
            public void onUpdate() {
                if (null == mRemoteViews)
                    onCreateRemoteViews();
                else {
                    WeatherInfo todayWeather = getTodayWeather();
                    IwdsLog.d(TAG, "todayWeather not is null : " + (null != todayWeather));
                    if (null != todayWeather) {
                        showInfoView(todayWeather);
                        // fillWeather(todayWeather);
                    }
                }
                IwdsLog.d(TAG, "update view for pkNames size:" + pkNameSet.size());
                for (String pkName : pkNameSet) {
                    updateWidgetForHost(pkName, mRemoteViews);
                    IwdsLog.d(TAG, "update view for pkName:" + pkName);
                }
            }
        });

        WeatherInfo today = getTodayWeather();
        if (null != today) {
            showInfoView(today);

        } else {
            hideInfoView();
        }
        return mRemoteViews;
    }

    private void hideInfoView() {
        IwdsLog.d(TAG, "today weather is false");
        mRemoteViews.setViewVisibility(R.id.no_info, View.VISIBLE);
        mRemoteViews.setViewVisibility(R.id.weather_info, View.GONE);
    }

    private void showInfoView(WeatherInfo today) {
        mRemoteViews.setViewVisibility(R.id.no_info, View.GONE);
        mRemoteViews.setViewVisibility(R.id.weather_info, View.VISIBLE);
        IwdsLog.d(TAG, "today weather is true");
        fillWeather(today);
    }

    public void fillWeather(WeatherInfo today) {

        mRemoteViews.setTextViewText(R.id.temperature, today.currentTemp + "℃");
        mRemoteViews.setTextViewText(R.id.descript, today.weather);
        mRemoteViews.setTextViewText(R.id.city, today.city);
        mRemoteViews.setImageViewResource(R.id.weather_icon, WeatherUtils.findImage(today.weatherCode));

        mRemoteViews.setTextViewText(R.id.max_temp, getApplicationContext().getString(R.string.max_temp) + today.maximumTemp + "℃");
        mRemoteViews.setTextViewText(R.id.min_temp, getApplicationContext().getString(R.string.min_temp) + today.minimumTemp + "℃");
    }

    public WeatherInfo getTodayWeather() {
        mForecasts = mDbHelper.getWeathersArrayFromDB();
        WeatherInfo today = null;
        if (null != mForecasts && mForecasts.length > 0) {
            for (int i = 0; i < mForecasts.length; i++) {
                WeatherInfo item = mForecasts[i];
                IwdsLog.d(TAG, "item city is : " + item.city);
                if (TODAY_TYPE == getItemViewType(i)) {
                    today = item;
                    break;
                }
            }
        }
        return today;
    }

    public int getItemViewType(int position) {
        int t = 0;

        if (mForecasts != null && mForecasts[0].dayIndex != 0) {
            t = 1;
        }

        return position == t ? TODAY_TYPE : OTHER_TYPE;
    }

    @Override
    protected void onAddedToHost(String hostPkg) {
        super.onAddedToHost(hostPkg);
        IwdsLog.i(this, "provider is added to host: " + hostPkg);
        pkNameSet.add(hostPkg);
    }

    private static final Set<String> pkNameSet = new HashSet<String>();

    @Override
    protected LocalRemoteViews onUpdate(LocalRemoteViews views) {
        IwdsLog.i(this, "Provider update");
        return views;
    }

    @Override
    protected void onDeletedFromHost(String hostPkg) {
        super.onDeletedFromHost(hostPkg);
        pkNameSet.remove(hostPkg);
    }
}
