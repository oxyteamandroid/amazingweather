package com.ingenic.weather.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.RemoteViews;

import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray.WeatherInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.weather.R;
import com.ingenic.weather.RefreshUtils;
import com.ingenic.weather.RefreshUtils.Listener;
import com.ingenic.weather.db.WeatherDbHelper;
import com.ingenic.weather.utils.WeatherUtils;

public class OriginWeatherWidgetProvider extends AppWidgetProvider {

    private static final String TAG = OriginWeatherWidgetProvider.class.getSimpleName();

    private RemoteViews mRemoteViews;
    private WeatherDbHelper mDbHelper;
    private WeatherInfo[] mForecasts;

    private final int TODAY_TYPE = 0;
    private final int OTHER_TYPE = 1;

    private ComponentName mComponent;

    @Override
    public void onEnabled(Context context) {
        IwdsLog.d(TAG, "onEnabled");
        super.onEnabled(context);
    }

    @Override
    public void onDisabled(Context context) {
        IwdsLog.d(TAG, "onDisabled");
        super.onDisabled(context);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        IwdsLog.d(TAG, "onDeleted");
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        mDbHelper = null != mDbHelper ? mDbHelper : new WeatherDbHelper(context.getApplicationContext(), "WATCH_WEATHER_DB", null, 1);
        IwdsLog.d(TAG, "onReceive");
        registerDateChangerListener(context);
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds) {
        IwdsLog.d(TAG, "onUpdate");
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        mComponent = null != mComponent ? mComponent : new ComponentName(context, OriginWeatherWidgetProvider.class);

        if (mRemoteViews == null) {
            IwdsLog.d(TAG, "create");
            mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_weather);
            hideInfoView();
            appWidgetManager.updateAppWidget(appWidgetIds, mRemoteViews);
        }

        refresh(context, appWidgetManager);
    }

    private void refresh(final Context context, final AppWidgetManager appWidgetManager) {
        RefreshUtils.refresh(context, new Listener() {

            @Override
            public void onUpdate() {
                queryTodayAndFill(context, appWidgetManager, mComponent);
            }

        });
        queryTodayAndFill(context, appWidgetManager, mComponent);
    }

    private void queryTodayAndFill(final Context context, final AppWidgetManager appWidgetManager, final ComponentName component) {
        WeatherInfo todayWeather = getTodayWeather();
        IwdsLog.d(TAG, "hh todayWeather not is null : " + (null != todayWeather));
        mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_weather);
        if (null != todayWeather) {
            showInfoView(todayWeather);
            fillWeather(context, todayWeather);
        } else {
            hideInfoView();
        }
        IwdsLog.d(TAG, "update view for pkNameID:" + component.toShortString());
        appWidgetManager.updateAppWidget(component, mRemoteViews);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            IwdsLog.d(TAG, "date changer");
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            mComponent = null != mComponent ? mComponent : new ComponentName(context, OriginWeatherWidgetProvider.class);
            refresh(context, appWidgetManager);
        }
    };

    private void registerDateChangerListener(Context context) {
        IwdsLog.d(TAG, "registerDateChangerListener");
        IntentFilter filter = new IntentFilter(Intent.ACTION_DATE_CHANGED);
        context.getApplicationContext().registerReceiver(mReceiver, filter);
    }

    private void hideInfoView() {
        mRemoteViews.setViewVisibility(R.id.no_info, View.VISIBLE);
        mRemoteViews.setViewVisibility(R.id.weather_info, View.GONE);
    }

    private void showInfoView(WeatherInfo today) {
        mRemoteViews.setViewVisibility(R.id.no_info, View.GONE);
        mRemoteViews.setViewVisibility(R.id.weather_info, View.VISIBLE);
    }

    public void fillWeather(Context context, WeatherInfo today) {
        mRemoteViews.setTextViewText(R.id.temperature, today.currentTemp + "℃");
        mRemoteViews.setTextViewText(R.id.descript, today.weather);
        mRemoteViews.setTextViewText(R.id.city, today.city);
        mRemoteViews.setImageViewResource(R.id.weather_icon, WeatherUtils.findImage(today.weatherCode));
        mRemoteViews.setTextViewText(R.id.max_temp, context.getString(R.string.max_temp) + today.maximumTemp + "℃");
        mRemoteViews.setTextViewText(R.id.min_temp, context.getString(R.string.min_temp) + today.minimumTemp + "℃");
    }

    public WeatherInfo getTodayWeather() {
        mForecasts = mDbHelper.getWeathersArrayFromDB();
        WeatherInfo today = null;
        if (null != mForecasts && mForecasts.length > 0) {
            for (int i = 0; i < mForecasts.length; i++) {
                WeatherInfo item = mForecasts[i];
                IwdsLog.d(TAG, "item city is : " + item.city);
                if (TODAY_TYPE == getItemViewType(i)) {
                    today = item;
                    break;
                }
            }
        }
        return today;
    }

    public int getItemViewType(int position) {
        int t = 0;
        if (mForecasts != null && mForecasts[0].dayIndex != 0) {
            t = 1;
        }
        return position == t ? TODAY_TYPE : OTHER_TYPE;
    }
}
