/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather;

import android.content.Intent;
import android.os.Bundle;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.weather.utils.WeatherUtils;

public class WeatherLineChartActivity extends RightScrollActivity {

    public static final String LINE_CHART_ACTION = "com.ingenic.weather.linechart";
    public static final String LINE_CHART_INFO = "forecasts";

    private RightScrollView mView;
    private WeatherLineChart mLineChart;
    private boolean mScreenisRound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = getRightScrollView();
        mScreenisRound = WeatherUtils.IsCircularScreen();
        if (mScreenisRound) {
            mView.setContentView(R.layout.weather_line_chart_layout_round);
            mView.setBackgroundResource(R.drawable.round);
        } else {
            mView.setContentView(R.layout.weather_line_chart_layout);
        }

        Intent intent = getIntent();
        if (intent != null && LINE_CHART_ACTION.equals(intent.getAction())) {
            WeatherInfoArray weathers = (WeatherInfoArray) intent
                    .getParcelableExtra(LINE_CHART_INFO);
            mLineChart = (WeatherLineChart) findViewById(R.id.weather_linechart);
            mLineChart.updateWeatherForecast(weathers.data);
        }
    }

}
