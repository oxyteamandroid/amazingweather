/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray;
import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray.WeatherInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.AmazingIndeterminateProgressBar;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.AmazingViewPager;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.iwds.widget.RightScrollView.OnRightScrollListener;
import com.ingenic.weather.adapter.WeatherPagerAdapter;
import com.ingenic.weather.db.WeatherDbHelper;
import com.ingenic.weather.utils.WeatherUtils;

/**
 * 天气应用
 * 
 * @author tZhang
 * 
 */
public class WeatherActivity extends RightScrollActivity implements
        OnRightScrollListener {

    private static final String TAG = "WeatherActivity";

    /**
     * 普通状态
     */
    private final static int WEATHER_NORMAL_STATE = 0;

    /**
     * 正在刷新状态
     */
    private final static int WEATHER_REFRESHING_STATE = 1;

    /**
     * 请求超时时间 15s
     */
    private final long WEATHER_RESULT_TIMEOUT_DELAY = 15 * 1000;

    /**
     * 通讯模型
     */
    private WeatherModel mWeatherModel;

    /**
     * 右滑View
     */
    private RightScrollView mRightScrollView;

    /**
     * 天气预报（ViewPager）
     */
    private AmazingViewPager mViewPager;

    /**
     * 天气预报的ViewPager适配器
     */
    private WeatherPagerAdapter mAdapter;

    /**
     * EmptyView
     */
    private View mEmptyview;

    /**
     * EmptyView提示语
     */
    private TextView mEmptyTip;

    /**
     * EmptyView图标
     */
    private ImageView mEmptyImage;

    /**
     * EmptyView刷新进度条
     */
    private AmazingIndeterminateProgressBar mEmptyPro;

    /**
     * 城市
     */
    private TextView mCity;

    /**
     * 天气预报发布时间
     */
    private TextView mUpdateTime;

    /**
     * 刷新按钮
     */
    private ImageButton mRefreshBtn;

    /**
     * 正在刷新圆形进度条
     */
    private AmazingIndeterminateProgressBar mProgress;

    /**
     * 天气数据库助手
     */
    private WeatherDbHelper mDbHelper;

    /**
     * 天气预报数据
     */
    private WeatherInfo[] mForecasts;
    private boolean mScreenisRound = false;

    /**
     * 是否正常连接
     */
    public static boolean sChannelAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 初始化
        init();

        mRefreshBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (mWeatherModel == null) {
                    IwdsLog.e(TAG, "WeatherModel is null, request failed!");
                    return;
                }

                mWeatherModel.requestRefresh();
                setRefreshButtonState(WEATHER_REFRESHING_STATE);
            }
        });

        // 更新界面
        updateUI();

    }

    /**
     * 初始化
     */
    private void init() {
        // 实例化右滑View
        mRightScrollView = getRightScrollView();
        mScreenisRound = WeatherUtils.IsCircularScreen();
        if (mScreenisRound) {
            mRightScrollView.setContentView(R.layout.weather_layout_round);
            mRightScrollView.setBackgroundResource(R.drawable.round);
        } else {
            mRightScrollView.setContentView(R.layout.weather_layout);
        }

        // 实例化主页
        mViewPager = (AmazingViewPager) mRightScrollView
                .findViewById(R.id.weather_viewpager);
        mViewPager.setTouchOrientation(AmazingViewPager.VERTICAL);

        // 实例化header
        mCity = (TextView) mRightScrollView.findViewById(R.id.city_name);
        mUpdateTime = (TextView) mRightScrollView
                .findViewById(R.id.update_time);
        mProgress = (AmazingIndeterminateProgressBar) mRightScrollView
                .findViewById(R.id.refresh_progress);
        mRefreshBtn = (ImageButton) mRightScrollView
                .findViewById(R.id.refresh_weather);

        // 实例化EmptyView
        mEmptyview = mRightScrollView.findViewById(R.id.emptyview);
        mEmptyTip = (TextView) mRightScrollView.findViewById(R.id.empty_tip);
        mEmptyImage = (ImageView) mRightScrollView
                .findViewById(R.id.empty_face);
        mEmptyPro = (AmazingIndeterminateProgressBar) mRightScrollView
                .findViewById(R.id.empty_progressBar);

        // 启动通讯模型
        mWeatherModel = WeatherModel.getInstance(this);
        mWeatherModel.startTransaction(mHandler);

        // 实例化数据库助手
        mDbHelper = new WeatherDbHelper(getApplicationContext(),
                "WATCH_WEATHER_DB", null, 1);
    }

    /**
     * 更新UI
     */
    private void updateUI() {
        // 获取数据库中的天气信息显示在界面上
        mForecasts = mDbHelper.getWeathersArrayFromDB();

        if (mForecasts == null) {
            // 没有数据
            if (mEmptyview != null && mEmptyview.getVisibility() == View.GONE) {
                mEmptyPro.stop();
                mEmptyPro.setVisibility(View.GONE);
                mEmptyview.setVisibility(View.VISIBLE);
            }
        } else if (mForecasts.length > 0) {
            if (mEmptyview != null
                    && mEmptyview.getVisibility() == View.VISIBLE) {
                mEmptyPro.stop();
                mEmptyPro.setVisibility(View.GONE);
                mEmptyview.setVisibility(View.GONE);
            }

            setRefreshButtonState(WEATHER_NORMAL_STATE);

            WeatherInfo info = mForecasts[0];

            // 接收到的第一个数据不是今天
            if (info.dayIndex != 0) {
                info = mForecasts[1];
                mViewPager.setDisplayedChild(1);
            }

            // 设置城市
            mCity.setText(info.city);

            // 设置发布时间
            String[] date = info.date.split("/");
            mUpdateTime.setText(String.format(getString(R.string.update_time),
                    getMonth(Integer.valueOf(date[0])), date[1],
                    info.updateTime));

            // 更新UI显示
            displayWeatherForecast(mForecasts);
        }
    }

    /**
     * 设置刷新按钮显示状态
     * 
     * @param state
     */
    private void setRefreshButtonState(int state) {
        if (mRefreshBtn != null && mProgress != null) {
            switch (state) {
            case WEATHER_NORMAL_STATE:
                mProgress.setVisibility(View.GONE);
                mProgress.stop();
                mRefreshBtn.setVisibility(View.VISIBLE);
                IwdsLog.d(TAG, "WEATHER_REFRESH_ENABLE_STATE");
                break;

            case WEATHER_REFRESHING_STATE:
                mProgress.setVisibility(View.VISIBLE);
                mProgress.start();
                mRefreshBtn.setVisibility(View.GONE);
                IwdsLog.d(TAG, "WEATHER_REFRESHING_STATE");
                break;
            default:
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 确保右滑退出功能正常
        if (mRightScrollView != null) {
            mRightScrollView.enableRightScroll();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // 停止通讯服务
        if (mWeatherModel != null) {
            mWeatherModel.stopTransaction(mHandler);
        }

    }

    /**
     * 显示天气预报信息
     * 
     * @param list
     */
    public void displayWeatherForecast(WeatherInfo[] array) {

        if (array != null) {
            if (mAdapter == null) {
                mAdapter = new WeatherPagerAdapter(this, mRightScrollView);
                mAdapter.setList(array);
                mViewPager.setAdapter(mAdapter);
            } else {
                mAdapter.setList(array);
                mAdapter.notifyDataChanged(array);
            }
        }
    }

    /**
     * 获取月份的国际化字符串
     * 
     * @param month
     * @return
     */
    private String getMonth(int month) {
        String[] months = getResources().getStringArray(R.array.month_entries);
        if (month <= 0 || month > months.length) {
            return months[0];
        } else {
            return months[month - 1];
        }
    }

    /**
     * 用来更新状态的Handler
     */
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {

            mHandler.removeMessages(WeatherModel.WEATHER_RESULT_TIMEOUT_STATE);

            int what = msg.what;

            mEmptyview.setEnabled(true);
            sChannelAvailable = true;

            switch (what) {
            case WeatherModel.WEATHER_DISCONNECTED_STATE:
                // 未连接/断开连接
                sChannelAvailable = false;
                setRefreshButtonState(WEATHER_NORMAL_STATE);

                break;

            case WeatherModel.WEATHER_REFRESH_FAILED_STATE:
                // 刷新失败
                setRefreshButtonState(WEATHER_NORMAL_STATE);

                if (mEmptyview != null
                        && mEmptyview.getVisibility() == View.VISIBLE) {
                    mEmptyTip.setText(R.string.request_weather_failed);
                    mEmptyImage.setVisibility(View.VISIBLE);
                    mEmptyPro.setVisibility(View.GONE);
                    mEmptyPro.stop();

                    // 设置按钮点击事件（点击重试）
                    mEmptyview.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (mWeatherModel != null) {
                                mWeatherModel.requestRefresh();
                                mEmptyview.setEnabled(false);
                            }
                        }
                    });
                } else {
                    AmazingToast.showToast(WeatherActivity.this,
                            R.string.request_weather_failed,
                            AmazingToast.LENGTH_LONG,
                            AmazingToast.BOTTOM_CENTER);
                }

                break;

            case WeatherModel.WEATHER_REFRESH_OK_STATE:
                // 刷新成功 接收到数据，保存到数据库
                int intsertRow = mDbHelper
                        .saveWeathersToDB(getResources(), ((WeatherInfoArray) (msg.obj)).data);

                // 保存失败
                if (intsertRow != 5) {
                    IwdsLog.d(TAG, "WEATHERINFO INSERT DATABASE FAILED!");
                    // return;
                }

                // 更新UI
                updateUI();

                // 提示用户
                AmazingToast.showToast(WeatherActivity.this,
                        R.string.refresh_success, AmazingToast.LENGTH_SHORT,
                        AmazingToast.BOTTOM_CENTER);

                break;

            case WeatherModel.WEATHER_REQUEST_STATE:
                // 请求天气数据
                mWeatherModel.requestRefresh();
                setRefreshButtonState(WEATHER_REFRESHING_STATE);
                break;

            case WeatherModel.WEATHER_RESULT_TIMEOUT_STATE:
                // 请求超时
                IwdsLog.d(TAG, "WEATHER REQUEST TIMEOUT!");

                if (mEmptyview != null
                        && mEmptyview.getVisibility() == View.VISIBLE) {
                    mEmptyTip.setText(R.string.request_timeout);
                    mEmptyImage.setVisibility(View.VISIBLE);
                    mEmptyPro.setVisibility(View.GONE);
                    mEmptyPro.stop();

                    // 设置按钮点击事件（点击重试）
                    mEmptyview.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (mWeatherModel != null) {
                                mWeatherModel.requestRefresh();
                                mEmptyview.setEnabled(false);
                            }
                        }
                    });

                } else {
                    setRefreshButtonState(WEATHER_NORMAL_STATE);

                    AmazingToast.showToast(WeatherActivity.this,
                            R.string.request_timeout, AmazingToast.LENGTH_LONG,
                            AmazingToast.BOTTOM_CENTER);
                }
                break;

            case WeatherModel.WEATHER_SEND_RESULT_FAILED_STATE:
                // 请求发送失败
                if (mEmptyview != null
                        && mEmptyview.getVisibility() == View.VISIBLE) {
                    mEmptyTip.setText(R.string.bluetooth_link_failed);
                    mEmptyImage.setVisibility(View.VISIBLE);
                    mEmptyPro.setVisibility(View.GONE);
                    mEmptyPro.stop();

                } else {
                    setRefreshButtonState(WEATHER_NORMAL_STATE);

                    AmazingToast.showToast(WeatherActivity.this,
                            R.string.bluetooth_link_failed,
                            AmazingToast.LENGTH_LONG,
                            AmazingToast.BOTTOM_CENTER);
                }

                break;

            case WeatherModel.WEATHER_SEND_RESULT_OK_STATE:
                // 请求发送成功
                if (mEmptyview != null
                        && mEmptyview.getVisibility() == View.VISIBLE) {
                    mEmptyTip.setText(R.string.loading_weather);
                    mEmptyImage.setVisibility(View.GONE);
                    mEmptyPro.setVisibility(View.VISIBLE);
                    mEmptyPro.start();

                } else {
                    AmazingToast.showToast(WeatherActivity.this,
                            R.string.loading_weather,
                            AmazingToast.LENGTH_SHORT,
                            AmazingToast.BOTTOM_CENTER);
                }

                // 设置请求超时
                mHandler.sendEmptyMessageDelayed(
                        WeatherModel.WEATHER_RESULT_TIMEOUT_STATE,
                        WEATHER_RESULT_TIMEOUT_DELAY);
                break;

            default:
                break;
            }

        };
    };

}
