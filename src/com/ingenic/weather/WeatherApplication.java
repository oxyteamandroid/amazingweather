/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather;

import android.app.Application;

/**
 * 天气预报Application
 * 
 * @author tZhang
 * 
 */
public class WeatherApplication extends Application {

    static final String TAG = "WeatherApplication";

    @Override
    public void onCreate() {
        super.onCreate();

        // 创建通讯模型
        WeatherModel.getInstance(this);
    }

}
