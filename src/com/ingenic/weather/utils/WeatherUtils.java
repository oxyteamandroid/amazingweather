/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather.utils;

import android.content.res.Resources;

import com.ingenic.iwds.HardwareList;
import com.ingenic.weather.R;

/**
 * 天气工具类
 * @author tZhang
 */

public class WeatherUtils {

    /**
     * 通过天气代码找到对应的天气图标
     * @param code
     * @return
     */
    public static int findImage(String code) {
        return R.drawable.yahoo00 + Integer.valueOf(code);
    }

    /**
     * 根据天气code查找对应的天气描述
     * @param resources
     * @param weatherCode
     * @return
     */
    public static String findWeather(Resources resources, String weatherCode) {
        int code = -1;
        try {
            code = Integer.parseInt(weatherCode);
        } catch (Exception e) {
            return resources.getString(R.string.wrong_weather);
        }
        String[] weathers = resources.getStringArray(R.array.weather_entries);
        if (code < 0 || code >= weathers.length) {
            return resources.getString(R.string.wrong_weather);
        } else {
            return weathers[code];
        }
    }

    private static long lastClickTime = 0;

    /**
     * 是否在一秒内重复点击
     * @return
     */
    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 1000) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    /**
     * 判断当前的屏是圆还是方形
     * @return
     */
    public static boolean IsCircularScreen(){
        return HardwareList.IsCircularScreen();
    }
}
