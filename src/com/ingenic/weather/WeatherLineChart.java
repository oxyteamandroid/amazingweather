/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.util.AttributeSet;
import android.view.View;

import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray.WeatherInfo;
import com.ingenic.weather.utils.WeatherUtils;

/**
 * 气温折线图
 *
 * @author tZhang
 */
public class WeatherLineChart extends View {

    private static final int GAP = 5;
    private WeatherInfo[] mForecasts;

    private int mIconSize = 52;
    private int[] mTempRange = new int[2];
    private int mWeekTextSize = 16;
    private int mWeatherTextSize = 15;
    private int mTempTextSize = 24;
    private int mDateTextSize = 24;

    private int normalTextColor = 0xFFFFFFFF;
    private int maxTempColor = 0xFFFF3000;
    private int minTempColor = 0xFF009CFF;

    private int tempCircleRadius = 8;

    /**
     * 画笔
     */
    private Paint mTextPaint;
    private Paint mLineChartPaint;

    /** 绘制虚线 */
    private Paint mPathPaint;
    private Path mPath;

    public WeatherLineChart(Context context) {
        this(context, null);
    }

    public WeatherLineChart(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeatherLineChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        this.setDrawingCacheEnabled(true);

        // 用来绘制文字的画笔
        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextAlign(Align.CENTER);

        // 用来绘制气温折线图的画笔
        BlurMaskFilter paintBGBlur = new BlurMaskFilter(1,
                BlurMaskFilter.Blur.INNER);
        mLineChartPaint = new Paint();
        mLineChartPaint.setAntiAlias(true);
        mLineChartPaint.setTextAlign(Paint.Align.CENTER);
        mLineChartPaint.setMaskFilter(paintBGBlur);
        mLineChartPaint.setStrokeWidth(2);
        mLineChartPaint.setStyle(Style.FILL);

        // 用来绘制虚线分隔线的画笔
        mPathPaint = new Paint();
        PathEffect effect = new DashPathEffect(new float[] { 2, 2, 2, 2 }, 1);
        mPathPaint.setColor(0xFF333333);
        mPathPaint.setStyle(Paint.Style.STROKE);
        mPathPaint.setAntiAlias(false);
        mPathPaint.setPathEffect(effect);
        mPath = new Path();

        // 重新计算最高温度和最低温度
        getTempRange(mTempRange);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mForecasts != null) {
            int count = mForecasts.length;
            if (count <= 0) {
                return;
            }

            int width = getWidth();
            int height = getHeight();
            int singleSize = width / 5;

            // 绘制竖向分隔线虚线
            drawPath(canvas, count, singleSize, height);

            // 绘制坐标横向天气信息的Text
            int top = 0;
            for (int i = 0; i < count; i++) {
                WeatherInfo info = mForecasts[i];
                if (info != null) {
                    top = drawInfo(canvas, info, singleSize, i);
                }
            }

            // 绘制气温折线图
            drawChart(canvas, top);

            canvas.save();
        }
    }

    private void drawPath(Canvas canvas, int count, int gap, int height) {
        for (int i = 1; i < count; i++) {
            mPath.moveTo(gap * i, 0);
            mPath.lineTo(gap * i, height);
            canvas.drawPath(mPath, mPathPaint);
        }
    }

    private int drawInfo(Canvas canvas, WeatherInfo info, int size, int current) {
        int left = size * current;
        int top = GAP;

        mTextPaint.setColor(normalTextColor);

        // 星期
        mTextPaint.setTextSize(mWeekTextSize);
        canvas.drawText(info.dayOfWeek.substring(0,
                info.dayOfWeek.length() >= 3 ? 3 : info.dayOfWeek.length()),
                left + size / 2, top + getBaseline(mTextPaint), mTextPaint);
        top += mWeekTextSize + GAP;

        // 日期
        mTextPaint.setTextSize(mDateTextSize);
        canvas.drawText(info.date.split("/")[1], left + size / 2, top
                + getBaseline(mTextPaint), mTextPaint);
        top += mDateTextSize + GAP;

        // 图标
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                WeatherUtils.findImage(info.weatherCode));
        canvas.drawBitmap(icon, left + (size - mIconSize) / 2, top, mTextPaint);
        top += mIconSize/* + GAP */;

        // 天气状况
        mTextPaint.setTextSize(mWeatherTextSize);
        canvas.drawText(info.weather.substring(0,
                info.weather.length() >= 4 ? 4 : info.weather.length()), left
                + size / 2, top + getBaseline(mTextPaint), mTextPaint);
        top += mWeatherTextSize + GAP;

        return top;
    }

    private static float getBaseline(Paint paint) {
        FontMetrics fontMetrics = paint.getFontMetrics();
        float size = paint.getTextSize();
        // 计算文字高度
        float fontHeight = fontMetrics.bottom - fontMetrics.top;
        // 计算文字baseline
        float textBaseY = size - (size - fontHeight) / 2 - fontMetrics.bottom;
        return textBaseY;
    };

    /***
     * 图片的缩放方法
     *
     * @param bgimage
     *            ：源图片资源
     * @param newWidth
     *            ：缩放后宽度
     * @param newHeight
     *            ：缩放后高度
     * @return
     */
    Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();

        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();

        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
                (int) height, matrix, true);
        return bitmap;
    }

    void drawChart(Canvas canvas, int top) {

        // 气温所对应的坐标
        float cx = 0f;
        float cy = 0f;
        float dx = 0f;
        float dy = 0f;
        int singleSize = getWidth() / 5;
        int count = mForecasts.length;
        int coordinateHeight = getHeight() - GAP * 5 - top;
        int spacePixel = (coordinateHeight - mTempTextSize * 2)
                / (mTempRange[1] - mTempRange[0]);
        int offsetTop = top + mTempTextSize;

        /******************* 绘制最高气温折线 **************************/
        mLineChartPaint.setColor(maxTempColor);
        mTextPaint.setColor(maxTempColor);

        for (int i = 0; i < count - 1; i++) {

            WeatherInfo info = mForecasts[i];

            int temp = info.maximumTemp;
            cx = singleSize * i + singleSize / 2;
            cy = offsetTop + (mTempRange[1] - temp) * spacePixel;
            dx = singleSize + cx;
            dy = offsetTop + (mTempRange[1] - mForecasts[i + 1].maximumTemp)
                    * spacePixel;
            String degress = getResources().getString(getDegressResource(info));

            // 绘制圆点和折线
            canvas.drawCircle(cx, cy, tempCircleRadius, mLineChartPaint);
            canvas.drawLine(cx, cy, dx, dy, mLineChartPaint);

            // 绘制温度
            mTextPaint.setTextSize(mTempTextSize);
            canvas.drawText(temp + degress, cx, cy + getBaseline(mTextPaint)
                    + tempCircleRadius, mTextPaint);
        }

        // 绘制最后一个最高气温
        canvas.drawCircle(dx, dy, tempCircleRadius, mLineChartPaint);

        WeatherInfo info = mForecasts[count - 1];
        String degress = getResources().getString(getDegressResource(info));
        canvas.drawText(info.maximumTemp + degress, dx, dy
                + getBaseline(mTextPaint) + tempCircleRadius, mTextPaint);
        /***********************************************************/

        /******************* 绘制最低气温折线 *************************/
        mLineChartPaint.setColor(minTempColor);
        mTextPaint.setColor(minTempColor);

        for (int j = 0; j < count - 1; j++) {
            info = mForecasts[j];
            int temp = info.minimumTemp;
            cx = singleSize * j + singleSize / 2;
            cy = offsetTop + (mTempRange[1] - temp) * spacePixel;
            dx = singleSize + cx;
            dy = offsetTop + (mTempRange[1] - mForecasts[j + 1].minimumTemp)
                    * spacePixel;

            // 绘制圆点和折线
            canvas.drawCircle(cx, cy, tempCircleRadius, mLineChartPaint);
            canvas.drawLine(cx, cy, dx, dy, mLineChartPaint);

            // 绘制温度
            degress = getResources().getString(getDegressResource(info));
            canvas.drawText(temp + degress, cx, cy + getBaseline(mTextPaint)
                    + tempCircleRadius, mTextPaint);
        }

        // 绘制最后一个最低气温
        canvas.drawCircle(dx, dy, tempCircleRadius, mLineChartPaint);

        info = mForecasts[count - 1];
        degress = getResources().getString(getDegressResource(info));
        canvas.drawText(info.minimumTemp + degress, dx, dy
                + getBaseline(mTextPaint) + tempCircleRadius, mTextPaint);
        /************************************************************/
    }

    public void updateWeatherForecast(WeatherInfo[] forecasts) {
        if (forecasts != null) {
            mForecasts = forecasts;
        }

        // 重新计算最高温度和最低温度
        getTempRange(mTempRange);
        invalidate();
    }

    private static int getDegressResource(WeatherInfo info) {
        return "c".equalsIgnoreCase(info.tempUnit) ? R.string.sheshidu
                : R.string.huashidu;
    }

    private int[] getTempRange(int[] temps) {
        if (temps == null) {
            temps = new int[2];
        }
        if (mForecasts != null && mForecasts.length > 0) {
            WeatherInfo info = mForecasts[0];
            temps[0] = info.minimumTemp;
            temps[1] = info.maximumTemp;
            for (int i = 1; i < mForecasts.length; i++) {
                info = mForecasts[i];
                int min = info.minimumTemp;
                int max = info.maximumTemp;
                if (temps[0] > min) {
                    temps[0] = min;
                }
                if (temps[1] < max) {
                    temps[1] = max;
                }
            }
        }
        return temps;
    }
}
