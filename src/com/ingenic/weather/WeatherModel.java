/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingWeather/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.weather;

import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.os.Handler;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.WeatherInfoArray;
import com.ingenic.iwds.datatransactor.elf.WeatherTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;

/**
 * 天气通讯回调
 * 
 * @author tZhang
 */
public class WeatherModel implements
        WeatherTransactionModel.WeatherInfoTransactionModelCallback {

    static final String TAG = "WeatherModel";

    /**
     * 未连接状态
     */
    public static final int WEATHER_DISCONNECTED_STATE = 0;

    /**
     * 发送请求状态
     */
    public static final int WEATHER_REQUEST_STATE = 1;

    /**
     * 请求超时状态
     */
    public static final int WEATHER_RESULT_TIMEOUT_STATE = 2;

    /**
     * 请求发送成功状态
     */
    public static final int WEATHER_SEND_RESULT_OK_STATE = 3;

    /**
     * 请求发送失败状态
     */
    public static final int WEATHER_SEND_RESULT_FAILED_STATE = 4;

    /**
     * 刷新成功状态
     */
    public static final int WEATHER_REFRESH_OK_STATE = 5;

    /**
     * 刷新失败状态
     */
    public static final int WEATHER_REFRESH_FAILED_STATE = 6;

    /**
     * 天气模型对象
     */
    private static WeatherModel mInstance;

    /**
     * 同步天气服务的UUID
     */
    private static final String UUID = "1dfe1c37-e619-2b74-4d09-03b56990a5fa";

    /**
     * 天气事物模型（通讯）
     */
    public static WeatherTransactionModel mTransactionModel;
    private static Set<Handler> handlers = new HashSet<Handler>();

    /**
     * 用来更新状态的Handler
     */
//    private Handler mHandler;

    /**
     * 构造单例对象
     * 
     * @param context
     * @return
     */
    public synchronized static WeatherModel getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new WeatherModel(context);
        }
        return mInstance;
    }

    /**
     * 构造器
     * 
     * @param context
     */
    private WeatherModel(Context context) {
        // 注册天气通讯的回调
        if (mTransactionModel == null) {
            mTransactionModel = new WeatherTransactionModel(context, this, UUID);
        }
    }

    /**
     * 启动通讯事物模型
     */
    public void startTransaction(Handler handler) {
        if (mTransactionModel == null) {
            IwdsLog.e(TAG, "WeatherTransactionModel is null, start failed!");
            return;
        }

//        mHandler = handler;
        handlers.add(handler);
        mTransactionModel.start();
        IwdsLog.d(TAG, "WeatherTransactionModel is start.");
    }

    /**
     * 关闭通讯事物模型
     * @param handler 
     */
    public void stopTransaction(Handler handler) {
        if (mTransactionModel == null) {
            IwdsLog.e(TAG, "WeatherTransactionModel is null, stop failed!");
            return;
        }
        handlers.remove(handler);
        if(handlers.isEmpty())
            mTransactionModel.stop();
        IwdsLog.d(TAG, "WeatherTransactionModel is stop.");
    }

    /**
     * 发送刷新请求
     */
    public void requestRefresh() {
        if (mTransactionModel == null) {
            IwdsLog.e(TAG, "WeatherTransactionModel is null, request failed!");
            return;
        }

        mTransactionModel.request();
        IwdsLog.d(TAG, "WEATHER REFRESHING...");
    }

    @Override
    public void onRequest() {

    }

    @Override
    public void onRequestFailed() {
        // 刷新失败
        for(Handler mHandler : handlers)
            mHandler.sendEmptyMessage(WEATHER_REFRESH_FAILED_STATE);
    }

    @Override
    public void onObjectArrived(WeatherInfoArray object) {
        if (object == null || object.data == null) {
            // 刷新失败，接收到数据的有误
            for(Handler mHandler : handlers)
                mHandler.sendEmptyMessage(WEATHER_REFRESH_FAILED_STATE);
            return;
        }

        // 保存成功，更新状态
        IwdsLog.d(TAG, object.toString());
        for(Handler mHandler : handlers)
            mHandler.obtainMessage(WEATHER_REFRESH_OK_STATE, object).sendToTarget();
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        IwdsLog.i(TAG, "Weather - Device : " + descriptor
                + " link connect is : " + isConnected);
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        IwdsLog.i(TAG, "Weather - channel available is : " + isAvailable);

        if (isAvailable) {
            // 正在刷新
            for(Handler mHandler : handlers)
                mHandler.sendEmptyMessage(WEATHER_REQUEST_STATE);
            IwdsLog.d(TAG, "SEND WEATHER REQUEST!");
        } else {
            // 未连接/断开连接
            for(Handler mHandler : handlers)
                mHandler.sendEmptyMessage(WEATHER_DISCONNECTED_STATE);
            IwdsLog.d(TAG, "WEATHER DT DISCONNECTED!");
        }

    }

    @Override
    public void onSendResult(DataTransactResult result) {
        if (result == null) {
            return;
        }

        int resultCode = result.getResultCode();
        if (resultCode == DataTransactResult.RESULT_OK) {
            // 请求发送成功
            for(Handler mHandler : handlers)
                mHandler.sendEmptyMessage(WEATHER_SEND_RESULT_OK_STATE);
            IwdsLog.d(TAG, "WEATHER SEND REQUEST OK!");
        } else {
            // 请求发送失败
            for(Handler mHandler : handlers)
                mHandler.sendEmptyMessage(WEATHER_SEND_RESULT_FAILED_STATE);
            IwdsLog.d(TAG, "WEATHER SEND REQUEST FAILED!");
        }

    }

}
